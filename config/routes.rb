Shackshop::Application.routes.draw do
  
  root to: 'orders#new'
  get '/orders', to: redirect('/')
  resources :orders
    
end
