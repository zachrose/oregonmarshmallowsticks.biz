class RemoveAddressCountryFromOrder < ActiveRecord::Migration
  def change
    remove_column :orders, :address_country
  end
end
