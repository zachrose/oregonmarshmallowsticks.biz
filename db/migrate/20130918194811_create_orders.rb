class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.string :address_line1
      t.string :address_line2
      t.string :address_city
      t.string :address_zip
      t.string :address_state
      t.string :address_country
      t.string :stripe_token
      t.integer :amount
      t.string :email
      t.string :status

      t.timestamps
    end
  end
end
