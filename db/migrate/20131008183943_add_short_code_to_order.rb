class AddShortCodeToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :short_code, :string
  end
end
