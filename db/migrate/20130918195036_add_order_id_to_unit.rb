class AddOrderIdToUnit < ActiveRecord::Migration
  def change
    change_table :units do |t|
      t.references :order, index: true
    end
  end
end
