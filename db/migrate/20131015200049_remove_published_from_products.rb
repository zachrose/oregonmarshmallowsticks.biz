class RemovePublishedFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :published
  end
end
