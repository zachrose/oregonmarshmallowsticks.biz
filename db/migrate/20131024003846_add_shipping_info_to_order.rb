class AddShippingInfoToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :shipping_method, :string
    add_column :orders, :tracking_number, :string
  end
end
