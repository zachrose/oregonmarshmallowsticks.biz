require "spec_helper"
require 'orders_helper'

include OrdersHelper

describe OrdersHelper do
  describe "#format_dollars" do
    it 'always has one ones place' do
      format_dollars(3).should == "0"
    end
  end
  
  describe "#format_cents" do
    it 'always has two decimal places' do
      format_cents(300).should == "00"
    end
  end
  
  describe "#format_dollars_and_cents" do
    it 'returns dollars in dollars and cents in cents' do
      format_dollars_and_cents(3995).should == "39.95"
    end
  end
  
end