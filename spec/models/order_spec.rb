require "spec_helper"
require "order"
require "product"
require "unit"

describe Order do
  
  describe "#valid?" do
    context "when it has no fields filled out" do
      it "is invalid" do
        subject.should_not be_valid
      end
    end
    context "when it has fields filled out" do
      subject do
        Order.new name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 1,
          stripe_token: 'example_token',
          email: '@example.museum'
      end

      it "is valid" do
        subject.should be_valid
      end
      
    end
  end
  
  describe "#product_name" do
    before(:each) do
      Product.create! name: "Rock",
        price: 299
    end
    context "when quantity is 1" do
      subject do
        Order.create! name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 1,
          email: 'email@example.com',
          stripe_token: "example_token"
      end
      it "is singular" do
        subject.product_name.should == "Rock"
      end
    end
    context "when quantity is >1" do
      subject do
        Order.create! name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 2,
          email: 'email@example.com',
          stripe_token: "example_token"
      end
      it "is plural" do
        subject.product_name.should == "Rocks"
      end
    end
  end
  
  describe "#short_code" do
    context "after initialization" do
      subject do
        Order.new name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 1,
          stripe_token: 'example_token',
          email: '@example.museum'
      end
      it "exists" do
        subject.short_code.should be_true
      end
      it "is is externally immutable" do
        expect { subject.short_code = "custom" }.to raise_error
      end
      it "is at least eight characters long and a mix of digits and letters" do
        subject.short_code.should match(/[a-zA-Z0-9]{8,}/)
      end
      it "persists" do
        code = subject.short_code
        subject.save!
        subject.reload
        subject.short_code.should == code
      end
    end
  end
  
  describe "#reserve_units!" do
    context "when units are available" do
      subject do
        Order.create! name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 1,
          stripe_token: 'example_token',
          email: '@example.museum'
      end
      it "reserves units" do
        p = Product.create! name: "Example", description: "...", price: 3995
        u = Unit.create!(product_id: 1)
        subject.reserve_units!
        u.reload
        u.order_id.should == subject.id
      end
    end
    
    context "when units are not available" do
      subject do
        Order.create! name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 1,
          stripe_token: 'example_token',
          email: '@example.museum'
      end
      it "raises an exception" do
        expect {subject.reserve_units!}.to raise_error
      end
    end  
  end
  
  describe "#unreserve_units" do
    context "after units have been reserved" do
      before do
        p = Product.create! name: "Example", description: "...", price: 3995
        u = Unit.create!(product_id: 1)
      end
      subject do
        Order.create! name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 1,
          stripe_token: 'example_token',
          email: '@example.museum'
      end
      it "unreserves units" do
        subject.reserve_units!
        u = Unit.first
        (u.order_id).should == subject.id
        subject.unreserve_units
        u.reload
        expect(u.order_id).to be_nil
      end
    end
  end
  
  describe "#total" do
    before do
      p = Product.create! name: "Marshmallow sticks", price: 3995
      2.times do Unit.create!({product_id: p.id}) end
    end
    context "with two units" do
      subject do
        Order.create! name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 2,
          stripe_token: 'example_token',
          email: '@example.museum'
      end
      it "gets the total right" do
        subject.total.should == (3995*2)
      end
    end
    context "with zero units" do
      subject do
        Order.create! name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 0,
          stripe_token: 'example_token',
          email: '@example.museum'
      end
      it "gets the total right" do
        subject.total.should == 0
      end
    end
  end
  
  describe "#charge" do
    context "with an acceptable chage" do
      before do
        p = Product.create! name: "Marshmallow sticks", price: 3995
        2.times do Unit.create!({product_id: p.id}) end
      end
      subject do
        Order.create! name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 1,
          email: 'email@example.com',
          stripe_token: "example_token"
      end
      it "calls creates a charge with the right arguments" do
        mock_charger = double()
        subject.stub(charger: mock_charger)
        mock_charger.should_receive(:create).with({
          amount: 3995,
          card: "example_token",
          description: 'email@example.com',
          currency: 'usd'
        })
        subject.charge!    
      end
    end
    context "with an insufficent chage" do
      subject do
        Order.create! name: "Bob Rose",
          address_line1: "5440 SW Southwood Dr",
          address_city: "Lake Oswego",
          address_zip: 97035,
          address_state: "OR",
          quantity: 0,
          stripe_token: 'example_token',
          email: '@example.museum'
      end
      it "should raise an exception" do
        expect {subject.charge!}.to raise_error
      end
    end
  end
end