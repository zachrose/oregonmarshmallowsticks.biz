require "spec_helper"
require "product"
require "unit"

describe Product do
  
  describe "self#one" do
    subject do
      Product
    end
    context "when there is one product" do
      it "will return that product" do
        p = Product.create!({
          name: "Sticks",
          description: ". . .",
          price: 3995
        })
        subject.one.should == p
      end
    end
    
    context "when there are zero products" do
      it "will return an empty collection" do
        subject.one.should nil
      end
    end
    
    context "when there are two products" do
      it "will return the more recent product" do
        p1 = Product.create!({
          name: "Sticks",
          description: ". . .",
          price: 3995
        })
        p2 = Product.create!({
          name: "Rocks",
          description: ". . .",
          price: 595
        })
        subject.one.should == p2
      end
    end
  end
  
  describe "self#find_by_name_like" do
    context "when such a similiarly-named product exists" do
      it "will return that product" do
        product = Product.create!({
          name: "Oregon Marshmallow Sticks",
          price: 3995
        })
        found = Product.find_by_name_like("sticks").first
        found.id.should == product.id
      end
    end
    context "when such a similarly-named product doesn't exist" do
      it "will return nil" do
        Product.find_by_name_like("bananas").should be_empty
      end
    end
  end
  
  describe "#available_units" do
    context "when it has available units" do
      subject do
        Product.create!({
          name: "Sticks",
          description: ". . .",
          price: 3995
        })
      end
      it "will return them" do
        u1 = Unit.create! product_id: subject.id
        u2 = Unit.create! product_id: subject.id
        subject.available_units.count.should == 2
        subject.available_units.first.id.should == u1.id
        subject.available_units.last.id.should == u2.id
      end
    end
    context "when it doesn't have available units" do
      subject do
        Product.create!({
          name: "Sticks",
          description: ". . .",
          price: 3995
        })
      end
      it "will return an empty collection" do
        subject.available_units.empty?.should be_true
      end
    end
  end
  
  describe "#available_count" do
    context "when it has available units" do
      subject do
        Product.create!({
          name: "Sticks",
          description: ". . .",
          price: 3995
        })
      end
      it "will return them" do
        u1 = Unit.create! product_id: subject.id
        u2 = Unit.create! product_id: subject.id
        subject.available_count.should == 2
      end
    end
    context "when it doesn't have available units" do
      subject do
        Product.create!({
          name: "Sticks",
          description: ". . .",
          price: 3995
        })
      end
      it "will return an empty collection" do
        subject.available_count.should == 0
      end
    end
  end
  
  describe "#available?" do
    context "when it has available units" do
      subject do
        Product.create!({
          name: "Sticks",
          description: ". . .",
          price: 3995
        })
      end
      it "will return true" do
        u1 = Unit.create! product_id: subject.id
        u2 = Unit.create! product_id: subject.id
        subject.available?.should == true
      end
    end
    context "when it doesn't have available units" do
      subject do
        Product.create!({
          name: "Sticks",
          description: ". . .",
          price: 3995
        })
      end
      it "will return false" do
        subject.available?.should == false
      end
    end
  end 
  
  
end

