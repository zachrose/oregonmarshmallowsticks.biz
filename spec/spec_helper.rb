require "active_record"
require "database_cleaner"

ActiveRecord::Base.establish_connection adapter: "sqlite3", database: ":memory:"

# This will break specs:
# ActiveRecord::Base.establish_connection adapter: "postgresql", database: "oms_test", host: "localhost"
# TODO: examine in more depth

# sqlite3 hates our mysql indexes
ActiveRecord::Migration.class_eval do
  def add_index *; end
  def enable_extension *; end
end

RSpec.configure do |config|
  config.filter_run :focus => true
  config.run_all_when_everything_filtered = true

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    silence_stream(STDOUT) do
      load "db/schema.rb"
    end
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end

$LOAD_PATH << "app/models"
$LOAD_PATH << "app/helpers"
