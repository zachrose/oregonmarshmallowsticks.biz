Given(/^I am on the homepage$/) do
  visit "/"
end

Given(/^there (?:is|are) (\d+) available units? of (.+)$/) do |quantity, product_name|
  p = Product.create!({name: product_name, price: 3995})
  quantity.to_i.times do
    Unit.create!({product_id: p.id})
  end
end

When "I fill in the following fields:" do |table|
  table.rows_hash.each do |field, value|
    fill_in field, with: value
  end
end

When(/^I select "(\S+)" as "(\S+)"$/) do |option, id|
  select option, :from => id
end

When(/^I press "(.*?)"$/) do |button|
  click_button button
end

Then(/^I should see that (\d+) units of (.+) are available$/) do |quantity, product_name|
  find('input#order_quantity')['max'].should == quantity
end

Then "I wait a few seconds" do
  # I swear, features fail without sleeping here.
  # Hard to debug. Mysterious. # TODO: Find out why
  sleep 6 # ?!
end

Then(/^I should see "(.*?)"$/) do |text|
  page.should have_text(text)
end

Then(/^there should not be an order form/) do
  page.assert_no_selector 'form'
end

Then(/^I should see the order form$/) do
  page.assert_selector 'form'
end

Then(/^there should be one order in the database with the following fields:$/) do |table|  
  actual = Order.first.attributes.select do |key, value| 
    attributes = %w(name address_line1 address_line2 address_city address_zip address_state email)
    attributes.include?(key)
  end
  table.rows_hash.should == actual
end

Then(/^there should be (\d+) available units? of marshmallow sticks$/) do |quantity|
  Unit.where("order_id IS NULL").count.should == quantity.to_i
end

Then(/^I should get a confirmation email at "(\S+)"$/) do |email_address|
  email = ActionMailer::Base.deliveries.first
  email.from.should == ["orders@oregonmarshmallowsticks.biz"]
  email.to.should == [email_address]
  email.subject.should include("Order Confirmation")
end

When(/^I refresh$/) do
  visit(current_path)
end

Then(/^I should be on the homepage$/) do
  current_path.should == '/'
end

# Composed steps

When "I start to order a marshmallow stick" do
  step "I start to order 1 marshmallow sticks"
end

When(/^I start to order (\d+) marshmallow sticks$/) do |quantity|
  step "I am on the homepage"
  step "I fill in the following fields:", table(%{
    | Name             | Micah             |
    | Address          | 625 NW Everett St |
    | Suite / Apt      | 347               |
    | City             | Porland           |
    | Email            | yay@example.com   |
    | ZIP              | 97209             |
    | Quantity         | #{quantity}       | 
    | Card Number      | 4242424242424242  |
    | CVC              | 123               |
  })
  step "I select \"Oregon\" as \"State\""
  step "I select \"01\" as \"expiration_month\""
  step "I select \"2020\" as \"expiration_year\""
end

When "I order a marshmallow stick with a bad card" do
  step "I am on the homepage"
  step "I fill in the following fields:", table(%{
    | Name             | Micah             |
    | Address          | 625 NW Everett St |
    | Suite / Apt      | 347               |
    | City             | Porland           |
    | Email            | yay@example.com   |
    | ZIP              | 97209             |
    | Quantity         | 1                 | 
    | Card Number      | 4000000000000002  |
    | CVC              | 123               |
  })
  step "I select \"Oregon\" as \"State\""
  step "I select \"01\" as \"expiration_month\""
  step "I select \"2020\" as \"expiration_year\""
  step "I press \"Order\""
  step "I wait a few seconds"
end

Given(/^I order a marshmallow stick$/) do
  step "I start to order a marshmallow stick"
  step "I press \"Order\""
  step "I wait a few seconds"
end

Given(/^I have just ordered a marshmallow stick$/) do
  step "I start to order a marshmallow stick"
  step "I press \"Order\""
  step "I wait a few seconds"
end


# Manipulators for edge cases

Given(/^a unit of (.+) costs (\d+) cents$/) do |product_name, cents|
  p = Product.find_by_name_like(product_name).first
  p.price = cents
  p.save!
end

When(/^someone else buys (?:a|an|that|those) (\S+) before me$/) do |a, product_name|
  product = Product.find_by_name_like(product_name).first
  unit = product.available_units.first
  unit.order_id = 666
  unit.save!
end

When(/^someone else buys (\d+) (\S+) before me$/) do |quantity, product_name|
  product = Product.find_by_name_like(product_name).first
  units = product.available_units.limit(quantity)
  units.each do |unit|
    unit.order_id = 666
    unit.save!
  end
end

Given "I am on a page that doesn't exist" do
  visit "/wraith"
end