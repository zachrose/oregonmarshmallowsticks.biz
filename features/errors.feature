Feature: User sees nice error pages

  @allow-rescue
  Scenario: User goes to a page that doesn't exist
    Given I am on a page that doesn't exist
    Then I should see "The page you want is not at this address. You may have mistyped the address or the page may have moved."