Feature: User orders marshmallow sticks

  Scenario: User sees how many marshmallow sticks are available
    Given there are 2 available units of marshmallow sticks
    Given I am on the homepage
    Then I should see that 2 units of marshmallow sticks are available
  
  Scenario: User sees that marshmallow sticks are sold out
    Given there are 0 available units of marshmallow sticks
    Given I am on the homepage
    Then I should see "SOLD OUT"
    And there should not be an order form
  
  Scenario: User orders marshmallow sticks
    Given there is 1 available unit of Oregon Marshmallow Sticks
    Given I am on the homepage
    When I fill in the following fields:
      | Name             | Micah             |
      | Address          | 625 NW Everett St |
      | Suite / Apt      | 347               |
      | City             | Porland           |
      | ZIP              | 97209             |
      | Email            | yay@example.com   |
      | Quantity         | 1                 | 
      | Card Number      | 4242424242424242  |
      | CVC              | 123               |
    And I select "Oregon" as "State"
    And I select "01" as "expiration_month"
    And I select "2020" as "expiration_year"
    And I press "Order"
    And I wait a few seconds
    Then I should see "THANK YOU FOR YOUR ORDER"
    And I should see "We have charged your card $39.95 for 1 units of Oregon Marshmallow Sticks"
    And I should see "We have sent a confirmation email to yay@example.com"
    And I should get a confirmation email at "yay@example.com"
    And there should be one order in the database with the following fields:
      | name             | Micah             |
      | address_line1    | 625 NW Everett St |
      | address_line2    | 347               |
      | address_city     | Porland           |
      | address_state    | OR                |
      | address_zip      | 97209             |
      | email            | yay@example.com   |
    And there should be 0 available units of marshmallow sticks
	
	Scenario: User is missing some shipping information
	  Given there is 1 available unit of marshmallow sticks
	  Given I am on the homepage
	  When I fill in the following fields:
	    | Name             | Micah             |
      | Quantity         | 1                 |
      | Card Number      | 4242424242424242  |
      | CVC              | 123               |
	  And I select "01" as "expiration_month"
	  And I select "2020" as "expiration_year"
	  And I press "Order"
	  And I wait a few seconds
	  Then I should see "Some required fields are missing"
	  And there should be 1 available units of marshmallow sticks
	
	Scenario: User orders marshmallow sticks with bad card info
	  Given there is 1 available unit of marshmallow sticks
    When I order a marshmallow stick with a bad card
    Then I should see "Your card could not be charged"
	  And there should be 1 available unit of marshmallow sticks
	
	Scenario: Two users try to buy the same unit
    Given there are 4 available units of marshmallow sticks
    When I start to order 3 marshmallow sticks
    And someone else buys 2 sticks before me
    And I press "Order"
    And I wait a few seconds
    Then I should see "Just now, somebody bought some of the products you wanted"
  
  Scenario: User places an order that doesn't meet the minimum charge
    Given there is 1 available unit of marshmallow sticks
    Given a unit of marshmallow sticks costs 25 cents
    When I order a marshmallow stick
    Then I should see "The minimum charge for this order was not met"
    And there should be 1 available unit of marshmallow sticks
  
  @wip
  Scenario: User refreshes after order
    Given there are 2 available unit of marshmallow sticks
    And I have just ordered a marshmallow stick
    When I refresh
    Then I should see the order form
    And I should be on the homepage