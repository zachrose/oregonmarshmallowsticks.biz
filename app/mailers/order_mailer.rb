class OrderMailer < ActionMailer::Base
  default from: "orders@oregonmarshmallowsticks.biz"
  helper :orders

  def confirmation_email(order)
    @product = Product.one
    @order = order
    mail to: @order.email, subject: "Order Confirmation (#{@order.short_code})"
  end
  
  def shipped_email(order)
    @product = Product.one
    @order = order
    mail to: @order.email, subject: "Order Shipped (#{@order.short_code})"
  end
  
end
