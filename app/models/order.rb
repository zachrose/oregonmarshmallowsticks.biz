class Order < ActiveRecord::Base
  
  # TODO: figure out how to add this back in without breaking tests
  # include FigLeaf
  # 
  # hide ActiveRecord::Base, ancestors: true, except: [
  #   Object, :find, :update, :errors, :valid?, :save]
    
  MinimumCharge = 50 # In cents, set by stripe
  
  class OutOfStockError < StandardError; end
  class InsufficientChargeError < StandardError; end
  
  has_many :units
  has_many :products, through: :units
  
  validates_presence_of(
    :name, :address_line1, :address_city, :address_zip, :quantity, :address_state,
    :stripe_token, :email, :quantity
  )
  
  after_initialize :set_short_code
  
  validates_numericality_of :quantity, { only_integer: true, greater_than_or_equal_to: 0}
  validates_format_of :address_zip, :with=>/(^\d{5}$)|(^\d{5}-\d{4}$)/
  
  def reserve_units!
    quantity = self.quantity.to_i
    Order.transaction do
      product_id = Product.one.id
      available = Unit.where({product_id: product_id, order_id: nil})
      if available.count >= quantity
        units = Unit.where({product_id: product_id, order_id: nil}).limit(quantity).update_all(order_id: self.id)
      else
        raise OutOfStockError
      end
    end
  end
  
  def unreserve_units
    Unit.where("order_id = ?", self.id).update_all(order_id: nil)
  end
  
  def total
    quantity = self.quantity.to_i
    price = Product.one.price
    quantity * price
  end
  
  def charger
    Stripe::Charge
  end
  
  def charge!
    if self.total < MinimumCharge
      raise InsufficientChargeError
    else
      self.charger.create(
        :amount       => self.total,
        :card         => self.stripe_token,
        :description  => self.email,
        :currency     => 'usd'
      )
    end
  end

  def product_name
    n = self.quantity == 1 ? Product.one.name : Product.one.name.pluralize
    return n
  end

  private
  
  def short_code=(val)
    self[:short_code] = val
  end
  
  def set_short_code
    unless self.short_code
      self.short_code = SecureRandom.hex(4)
    end
  end
  
end