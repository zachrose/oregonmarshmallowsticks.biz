class Product < ActiveRecord::Base
  
  # TODO: figure out how to add this back in without breaking tests
  # include FigLeaf
  # 
  # hide ActiveRecord::Base, ancestors: true, except: [
  #   Object, :find, :update, :errors, :valid?, :save]
  
  has_many :units
  has_many :orders, through: :units
  
  validates_presence_of :name, :price
  
  # Just return all the products, and look up how many of them have no order_id
  def available_units
    Unit.where("product_id = ? AND order_id IS NULL", self.id)
  end
  
  def available_count
    Unit.where("product_id = ? AND order_id IS NULL", self.id).length
  end
  
  def available?
    !Unit.where("product_id = ? AND order_id IS NULL", self.id).empty?
  end
  
  # 'The product that should be for sale', which for now is just an alias for #last
  def self.one
    Product.last
  end
  
  def self.find_by_name_like(name)
    fuzzed = "%#{name}%"
    Product.where('upper(name) like ?', fuzzed.upcase)
  end
  
end
