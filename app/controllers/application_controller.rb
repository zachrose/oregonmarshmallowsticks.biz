class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def self.expose *methods
    methods.each do |method|
      attr_reader method
      helper_method method
    end
  end
end
