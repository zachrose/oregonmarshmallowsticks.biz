class ProductsController < ApplicationController
  
  def show
    @products = Product.all_with_quantities_available
  end
  
end
