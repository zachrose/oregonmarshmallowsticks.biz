class OrdersController < ApplicationController
  expose :order

  def new
    flash[:alert] = []
    flash[:notice] = []
    @product = Product.one
    @order = Order.new
    render :new
  end
  
  def create
    flash[:alert] = []
    flash[:notice] = []
    @product = Product.one
    @available_count = Product.one.available_units.length
    @order = Order.new(order_params)
    if @order.save
      begin
        @order.reserve_units!
      rescue Order::OutOfStockError
        flash[:alert] << "Just now, somebody bought some of the products you wanted. Please try again."
        @body_class = 'alert'
        render :new, alert: alert
        return
      end
      begin
        @order.charge!
      rescue Order::InsufficientChargeError
        @order.unreserve_units
        flash[:alert] << "The minimum charge for this order was not met. We can only process orders for more than 50 cents."
        @body_class = 'alert'
        render :new, alert: alert
        return
      rescue Stripe::CardError
        @order.unreserve_units
        flash[:alert] << "Your card could not be charged. Please verify your information and try again."
        @body_class = 'alert'
        render :new
        return
      end
      OrderMailer.confirmation_email(@order).deliver
      render :create
    else
      render :new
    end
  end

  def demo
    @product = Product.one
    @available_count = 42
    @order = Order.new
    flash[:alert] = []
    flash[:notice] = []
    flash[:alert] << "Your card could not be charged. Please verify your information and try again. This is a really long flash alert. Wow! just look at it go, will you?"
    flash[:alert] << "Yowza, here's another one."
    flash[:notice] << "This is just a notice."
    if !flash[:alert].empty? || !flash[:notice].empty?
      @body_class = 'alert'
    end
    render :new
  end

  private

  # I heard this was a good idea...
  def order_params
    params.require(:order).permit(
      :name, :address_line1, :address_line2, :address_city, :address_zip, :address_state,
      :quantity, :stripe_token, :email
    )
  end
end