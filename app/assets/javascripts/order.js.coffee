jQuery ->
  return abort() unless window.Stripe
  Stripe.setPublishableKey($('meta[name="stripe-key"]').attr('content'))
  order.setupForm()

order =
  setupForm: ->
    $('#new_order').submit (e)->
      e.preventDefault()
      $('input[type=submit]').attr('disabled', true)
      order.processCard()

  processCard: ->
    card =
      number: $('#card_number').val()
      cvc: $('#cvc').val()
      expMonth: $('#expiration_month').val()
      expYear: $('#expiration_year').val()
    Stripe.createToken(card, order.handleStripeResponse)

  handleStripeResponse: (status, response) ->
    if status == 200
      $('body').removeClass('alert')
      $('#stripe_token').val(response.id)
      $('#new_order')[0].submit()
    else
      $('input[type=submit]').attr('disabled', false)
      $('#messages').empty().append("<div class='alert'>#{response.error.message}</div>")
      $('body').addClass('alert')
      code = response.error?.code
      switch code
        when 'invalid_number' || 'incorrect_number' then stripe_warn('card_number')
        when 'invalid_expiry_month' || 'invalid_expiry_year' then stripe_warn('expiration')
        when 'invalid_cvc' || 'incorrect_cvc' then stripe_warn('cvc')
        
stripe_warn = (input)->
  if window.repaintBackground then window.repaintBackground()
  $(".input.#{input} span.error")
    .remove()
  $(".input.#{input}")
    .addClass('field_with_errors')
    .remove('span.error')
    .append("<span class='error'>invalid</span>")
  
  

abort = ->
  $('#messages').append('<div class="alert">Our credit card machine is on the fritz. Please try again in a few minutes.</div>')
  $('#new_order input[type=submit]').attr('disabled', true)