document.addEventListener "page:change", setBodyClass

$ ->
  setBodyClass()
  
setBodyClass = ->
  $('body').attr('class', '')
  for message in $('#messages div')
    $('body').addClass $(message).attr('class')
  repaintBackground();
  
window.repaintBackground = ->
  $("body").hide()
  setTimeout(( -> $("body").show()), 1)